<?php

namespace scratch;

use Illuminate\Database\Eloquent\Model;

class Postfile extends Model
{
    protected $fillable = ['location', 'user_id', 'commentable_id', 'commentable_type'];

    public function user()
    {
      return $this->belongsTo('scratch\User');
    }

    public function commentable()
    {
      return $this->morphTo();
    }
}
