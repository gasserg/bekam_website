<?php

namespace scratch;

use Illuminate\Database\Eloquent\Model;

class Realestate extends Model
{
    protected $fillable =
    ['city', 'location', 'area', 'price', 'description',
     'user_id', 'post_type', 'unit_type'];

    public function user()
    {
      return $this->belongsTo('scratch\User');
    }

    public function comments()
    {
      return $this->morphMany('scratch\Comment', 'commentable');
    }

    public function postfiles()
    {
      return $this->morphMany('scratch\Postfile', 'commentable');
    }

    public function saves()
    {
      return $this->morphMany('scratch\Save', 'likable');
    }
}
