<?php

namespace scratch;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    protected $fillable = [
      'amount', 'user_id', 'expiry_days'
    ];

    public function user()
    {
      return $this->belongsTo('scratch\User');
    }

    public static function mark($user_id)
    {
      // Stores a negative point for one week
      return static::create([
        'amount' => -1,
        'user_id' => $user_id,
        'expiry_days' => 7,
      ])->save();
    }
}
