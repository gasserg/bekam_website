<?php

namespace scratch\Http\Controllers;

use Illuminate\Http\Request;
use scratch\Car;
use scratch\Realestate;

class SitemapController extends Controller
{
    public function index()
    {
      return response()->view('sitemap.index', [
      ])->header('Content-Type', 'text/xml');
    }

    public function main()
    {
      return response()->view('sitemap.main', [
      ])->header('Content-Type', 'text/xml');
    }

    public function cars()
    {
      $cars = Car::orderBy('created_at', 'desc')->get();

      return response()->view('sitemap.cars', [
          'cars' => $cars,
      ])->header('Content-Type', 'text/xml');
    }
    public function realestate()
    {
      $realestates = Realestate::orderBy('created_at', 'desc')->get();

      return response()->view('sitemap.realestate', [
          'realestates' => $realestates,
      ])->header('Content-Type', 'text/xml');
    }
}
