<?php

namespace scratch\Http\Controllers;

use Illuminate\Http\Request;

class AboutAndContactsController extends Controller
{
  public function index()
  {
      return view('about_and_contacts', ['page_title' => 'About Us and Contacts', ]);
  }
}
