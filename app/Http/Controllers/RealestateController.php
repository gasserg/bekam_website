<?php

namespace scratch\Http\Controllers;

use scratch\Realestate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Storage;
use scratch\Postfile;

class RealestateController extends Controller
{
    public function index(Request $request)
    {
      $request->flash();
      $realestates = Realestate::orderBy('created_at', 'desc');
      $append_params = [];
      $data = $request->validate([
        'q' => 'nullable',
        'min-price' => 'nullable|integer',
        'max-price' => 'nullable|integer',
        'min-area' => 'nullable|integer',
        'max-area' => 'nullable|integer',
        'unit-type' => 'nullable',
        'post-type' => 'nullable',
        'city' => 'nullable',
        'location' => 'nullable',
      ]);
      if($request->exists('q'))
      {
        $append_params['q'] = $request->q;
        $sql = " 1 = 1 ";
        $params = [];
        foreach (explode(' ', $request->q) as $q) {
          $sql .= " AND (description Like ? OR city Like ? OR location like ?) ";
          $params[] = "%".$q."%";
          $params[] = "%".$q."%";
          $params[] = "%".$q."%";
        }
        $realestates = $realestates->whereRaw($sql, $params);
      }

      if($request->has('min-price') and $data['min-price'] != ""){
        $realestates =  $realestates->where('price', '>', $data['min-price']);
        $append_params['min-price'] = $data['min-price'];
      }
      if($request->has('max-price') and $data['max-price'] != ""){
        $realestates =  $realestates->where('price', '<', $data['max-price']);
        $append_params['max-price'] = $data['max-price'];
      }

      if($request->has('min-area') and $data['min-area'] != ""){
        $realestates =  $realestates->where('area', '>', $data['min-area']);
        $append_params['min-area'] = $data['min-area'];
      }
      if($request->has('max-area') and $data['max-area'] != ""){
        $realestates =  $realestates->where('area', '<', $data['max-area']);
        $append_params['max-area'] = $data['max-area'];
      }
      if($request->has('unit-type') and $data['unit-type'] != ""){
        $realestates =  $realestates->where('unit_type', $data['unit-type']);
        $append_params['unit-type'] = $data['unit-type'];
      }
      if($request->has('post-type') and $data['post-type'] != ""){
        $realestates =  $realestates->where('post_type', $data['post-type']);
        $append_params['post-type'] = $data['post-type'];
      }
      if($request->has('city') and $data['city'] != ""){
        $realestates =  $realestates->where('city', $data['city']);
        $append_params['city'] = $data['city'];
      }
      if($request->has('location') and $data['location'] != ""){
        $realestates =  $realestates->where('location', $data['location']);
        $append_params['location'] = $data['location'];
      }

      $realestates = $realestates->paginate(10)->appends($append_params);

      // if($request->page != null){
      //   return redirect()->route('login');
      // } else {
        return view('realestate.index', ['realestates' => $realestates, 'page_title' => 'Realestate', ]);
      // }
    }

    public function myrealestate(Request $request)
    {
      if(Auth::check())
      {
        $realestates = Auth::user()->realestates()->orderBy('created_at', 'desc')->paginate(10);
        return view('realestate.index', ['realestates' => $realestates, 'page_title' => 'My Realestate', ]);
      } else {
        return redirect()->route('login');
      }
    }

    public function saved(Request $request)
    {
      if(Auth::check())
      {
        $realestates = Realestate::whereHas('saves', function($q)
        {
            $q->where('user_id', Auth::user()->id);
        })->paginate(10);
        return view('realestate.index', ['realestates' => $realestates,  'page_title' => 'Saved Realestate', ]);
      } else {
        return redirect()->route('login');
      }
    }

    public function create()
    {
      if(Auth::check())
      {
        return view('realestate.create', ['page_title' => 'Create Realestate AD', ]);
      } else {
        return redirect()->route('login');
      }
    }

    public function store(Request $request)
    {
      $data = $request->validate([
        'city' => 'required|string',
        'location' => 'required|string',
        'area' => 'required|integer|min:40|max:5000',
        'price' => 'required|integer|min:500|max:500000000',
        'description' => 'required|string',
        'post_type' => 'required|string|in:sale,rent',
        'unit_type' => 'required|string',
      ]);
      $realestate = new Realestate($data);
      $realestate->save();
      // Save the file
      $files = $request->file('files');
      if($files)
      {
        foreach($files as $file)
        {
          Storage::disk('public')->put('realestate'.$realestate->id, $file);
          Postfile::create([
            'location' => DIRECTORY_SEPARATOR.'realestate'.$realestate->id.DIRECTORY_SEPARATOR.$file->hashName(),
            'user_id' => Auth::id(),
            'commentable_id' => $realestate->id,
            'commentable_type' => 'scratch\Realestate',
          ]);
        }
      }
      return view('realestate.store');
    }

    public function show(Realestate $realestate)
    {
        // increment the number of Views
        $realestate->increment('views');
        return view('realestate.show',
        [
          'realestate' => $realestate,
          'page_title' => 'Realestate ' .$realestate->unit_type . " for " . $realestate->post_type . " : " . $realestate->description,
        ]);
    }

    public function edit(Realestate $realestate)
    {
        //
    }

    public function update(Request $request, Realestate $realestate)
    {
        //
    }

    public function destroy(Realestate $realestate)
    {
      $realestate->delete();
      return view('realestate.destroy');
    }

    // Returns the number of realestate units available available
    // This is to be used for the back end
    public function count()
    {
        return ['data' => Realestate::all()->count(), ];
    }
}
