<?php

namespace scratch\Http\Controllers;

use scratch\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use scratch\Notifications\CommentMade;

class CommentController extends Controller
{
    public function store(Request $request)
    {
      if(Auth::check())
      {
        $data = $request->validate([
          'body' => 'required',
          'commentable_id' => 'required',
          'commentable_type' => 'required',
        ]);
        $data['user_id'] = Auth::id();
        // Create the Comment
        $comment = Comment::create($data);
        // Notify the Post owner
        if($comment->commentable->user)
        {
          $comment->commentable->user->notify(new CommentMade($comment));
        }
        return back();
      } else {
        return redirect()->route('login');
      }
    }

    public function destroy(Comment $comment)
    {
        $comment->delete();
        return back();
    }
}
