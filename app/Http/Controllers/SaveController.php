<?php

namespace scratch\Http\Controllers;

use Illuminate\Http\Request;

use scratch\Save;
use Illuminate\Support\Facades\Auth;

class SaveController extends Controller
{
    public function store(Request $request)
    {
      if(Auth::check())
      {
        $data = $request->validate([
          'likable_id' => 'required',
          'likable_type' => 'required',
        ]);
        $data['user_id'] = Auth::id();
        Save::create($data);
        return back();
      } else {
        return redirect()->route("login");
      }
    }

    public function destroy($id)
    {
        Save::findorfail($id)->delete();
        return back();
    }
}
