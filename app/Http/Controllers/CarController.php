<?php

namespace scratch\Http\Controllers;

use scratch\Car;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Storage;
use scratch\Postfile;

class CarController extends Controller
{
    public function index(Request $request)
    {
        $request->flash();
        $cars = Car::orderBy('created_at', 'desc');
        $append_params = [];
        $data = $request->validate([
          'q' => 'nullable',
          'min-price' => 'nullable|integer',
          'max-price' => 'nullable|integer',
          'min-year' => 'nullable|integer',
          'max-year' => 'nullable|integer',
          'make' => 'nullable',
          'model' => 'nullable',
        ]);
        if($request->exists('q'))
        {
          $append_params['q'] = $request->q;
          $sql = " 1 = 1 ";
          $params = [];
          foreach (explode(' ', $request->q) as $q) {
            $sql .= " AND (description Like ? OR model Like ? OR make like ?) ";
            $params[] = "%".$q."%";
            $params[] = "%".$q."%";
            $params[] = "%".$q."%";
          }
          $cars = $cars->whereRaw($sql, $params);
        }

        if($request->has('min-price') and $data['min-price'] != ""){
          $cars =  $cars->where('price', '>', $data['min-price']);
          $append_params['min-price'] = $data['min-price'];
        }
        if($request->has('max-price') and $data['max-price'] != ""){
          $cars =  $cars->where('price', '<', $data['max-price']);
          $append_params['max-price'] = $data['max-price'];
        }

        if($request->has('min-year') and $data['min-year'] != ""){
          $cars =  $cars->where('year', '>', $data['min-year']);
          $append_params['min-year'] = $data['min-year'];
        }
        if($request->has('max-year') and $data['max-year'] != ""){
          $cars =  $cars->where('year', '<', $data['max-year']);
          $append_params['max-year'] = $data['max-year'];
        }
        if($request->has('make') and $data['make'] != ""){
          $cars =  $cars->where('make', $data['make']);
          $append_params['make'] = $data['make'];
        }
        if($request->has('model') and $data['model'] != ""){
          $cars =  $cars->where('model', $data['model']);
          $append_params['model'] = $data['model'];
        }

        $cars = $cars->paginate(10)->appends($append_params);
        // if($request->page != null and !Auth::check()){
        //   return redirect()->route('login');
        // } else {
          return view('cars.index', ['cars' => $cars, 'page_title' => 'Cars', ]);
        // }
    }

    public function mycars(Request $request)
    {
      if(Auth::check())
      {
        $cars = Auth::user()->cars()->orderBy('created_at', 'desc')->paginate(10);
        return view('cars.index', ['cars' => $cars, 'page_title' => 'My Cars', ]);
      } else {
        return redirect()->route('login');
      }
    }

    public function saved(Request $request)
    {
      if(Auth::check())
      {
        $cars = Car::whereHas('saves', function($q)
        {
            $q->where('user_id', Auth::user()->id);
        })->paginate(10);
        return view('cars.index', ['cars' => $cars, ]);
      } else {
        return redirect()->route('login');
      }
    }

    public function create()
    {
        if(Auth::check())
        {
          return view('cars.create', ['page_title' => 'Create Car AD', ]);
        } else {
          return redirect()->route('login');
        }
    }

    public function store(Request $request)
    {
      if(Auth::check())
      {
        $data = $request->validate([
          'make' => 'required|string',
          'model' => 'required|string',
          'year' => 'required|integer|min:1930|max:2018',
          'price' => 'required|integer|min:500|max:10000000',
          'description' => 'required|string',
        ]);
        $data['user_id'] = Auth::id();
        $car = new Car($data);
        $car->save();
        // Save the file
        $files = $request->file('files');
        if($files)
        {
          foreach($files as $file)
          {
            Storage::disk('public')->put('car'.$car->id, $file);
            Postfile::create([
              'location' => DIRECTORY_SEPARATOR.'car'.$car->id.DIRECTORY_SEPARATOR.$file->hashName(),
              'user_id' => Auth::id(),
              'commentable_id' => $car->id,
              'commentable_type' => 'scratch\Car',
            ]);
          }
        }
        return view('cars.store');
      } else {
          return redirect()->route('login');
      }
    }

    public function show(Car $car)
    {
        // Increment the number of Views
        $car->increment('views');
        return view('cars.show', ['car' => $car, 'page_title' => 'Used Car for Sale' . $car->description , ]);
    }

    public function edit(Car $car)
    {
        //
    }

    public function update(Request $request, Car $car)
    {
        //
    }

    public function destroy(Car $car)
    {
        $car->delete();
        return view('cars.destroy');
    }

    // Returns the number of cars available
    public function count()
    {
        return ['data' => Car::all()->count(), ];
    }
}
