<?php

namespace scratch\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use scratch\User;


class UserController extends Controller
{
    public function show($id)
    {
      if(Auth::check())
      {
        $user = User::with('saves', 'cars', 'realestates')->findorfail($id);
        return view('user.show', ['user' => $user,  'page_title' => $user->name,  ]);
      } else {
        return redirect()->route('login');
      }
    }

    public function edit()
    {
        // Show the form to edit user's profile
        if(Auth::check()){
          return view("user.edit");
        } else {
          return redirect()->route('login');
        }
    }

    public function update(Request $request)
    {
        // update the user
        if(Auth::check()){
          $user = Auth::user();
          $request->flash();

          $validators = [
            'name' => 'required|string|max:255',
            'phone' => 'filled',
          ];
          if($request->input('email') != $user->email)
          {
            $validators['email'] = 'required|string|email|max:255|unique:users';
          }
          $data = $request->validate($validators);

          if(isset($data['name'])){$user->name = $data['name'];}
          if(isset($data['email'])){$user->email = $data['email'];}
          if(isset($data['phone'])){$user->phone = $data['phone'];}
          $user->save();

          return view("user.edit", ['user_edit_success' => true, ]);
        } else {
          return redirect()->route('login');
        }
    }
}
