<?php

namespace scratch\Http\Controllers\api;

use scratch\Car;
use Illuminate\Http\Request;
use scratch\Http\Controllers\Controller;

class CarController extends Controller
{
    public function indexMakes()
    {
      $makes = Car::select('make')->distinct()->orderBy('make')->get();
      return $makes;
    }

    public function indexModels(Request $request)
    {
      $data = $request->validate([
        'make' => "required",
      ]);
      $make = $data['make'];
      $models = Car::select('model')->where('make', $make)->distinct()->orderBy('model')->get();
      return $models;
    }
}
