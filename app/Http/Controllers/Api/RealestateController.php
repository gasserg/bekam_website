<?php

namespace scratch\Http\Controllers\Api;

use scratch\Realestate;
use Illuminate\Http\Request;
use scratch\Http\Controllers\Controller;

class RealestateController extends Controller
{
  public function indexCities()
  {
    $cities = Realestate::select('city')->distinct()->orderBy('city')->get();
    return $cities;
  }

  public function indexLocations(Request $request)
  {
    $data = $request->validate([
      'city' => "required",
    ]);
    $city = $data['city'];
    $locations = Realestate::select('location')->where('city', $city)->distinct()->orderBy('location')->get();
    return $locations;
  }
}
