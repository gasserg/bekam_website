<?php

namespace scratch\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function index()
    {
      $notifications = Auth::user()->notifications()->paginate(10);

      return view('notifications.index', [
        'notifications' => $notifications, 'page_title' => 'Notifications', 
      ]);
    }
}
