<?php

namespace scratch\Http\Middleware;

use Closure;

class Lang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if(!in_Array($request->segment(1), ['en', 'ar', ])){
        return redirect('/ar/'.$request->path());
      } else {
        return $next($request);
      }
    }
}
