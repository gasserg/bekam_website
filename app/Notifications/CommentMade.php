<?php

namespace scratch\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class CommentMade extends Notification
{
    use Queueable;

    public function __construct($comment)
    {
      $this->comment = $comment;
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function toArray($notifiable)
    {
      if($this->comment->commentable_type == 'scratch\Car')
      {
        $url = 'cars/'.$this->comment->commentable_id;
      } elseif($this->comment->commentable_type == 'scratch\Realestate')
      {
        $url = 'realestate/'.$this->comment->commentable_id;
      } else {
        $url = null;
      }

      return [
          'message' => 'A comment was made on one of your posts',
          'url' => $url,
      ];
    }
}
