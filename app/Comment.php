<?php

namespace scratch;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['user_id', 'commentable_id', 'commentable_type', 'body'];

    public function user()
    {
      return $this->belongsTo('scratch\User');
    }

    public function commentable()
    {
      return $this->morphTo();
    }
}
