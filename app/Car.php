<?php

namespace scratch;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable = ['make', 'model', 'year', 'price', 'description', 'user_id'];

    public function user()
    {
      return $this->belongsTo('scratch\User');
    }

    public function comments()
    {
      return $this->morphMany('scratch\Comment', 'commentable');
    }

    public function postfiles()
    {
      return $this->morphMany('scratch\Postfile', 'commentable');
    }

    public function saves()
    {
      return $this->morphMany('scratch\Save', 'likable');
    }

    public function getLogoAttribute()
    {

      $logo_refs = [
        'Lexus' => "https://upload.wikimedia.org/wikipedia/en/thumb/d/d1/Lexus_division_emblem.svg/491px-Lexus_division_emblem.svg.png",
        'Saipa' => "https://upload.wikimedia.org/wikipedia/en/c/ca/Saipa_Logo_New.png",
        'Skoda' => "https://upload.wikimedia.org/wikipedia/en/b/b5/Skoda_Auto_logo_%282011%29.svg",
        'Speranza' => "http://www.speranzaegypt.com/wp-content/uploads/2014/12/LOGO-copy-21.png",
        'Subaru' => "https://upload.wikimedia.org/wikipedia/en/f/fe/Subaru_logo_and_wordmark.svg",
        'Suzuki' => "https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Suzuki_Motor_Corporation_logo.svg/512px-Suzuki_Motor_Corporation_logo.svg.png",
        'Proton' => "https://upload.wikimedia.org/wikipedia/en/4/4f/PROTON_logo_%282016_%E2%80%93_present%29.png",
        'Seat' => "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/SEAT_Logo_from_2017.svg/913px-SEAT_Logo_from_2017.svg.png",
        'Geely' => "https://upload.wikimedia.org/wikipedia/en/d/d8/Geely_logo.png",
        'Porsche' => "https://upload.wikimedia.org/wikipedia/en/7/73/Porsche_logotype.png",
        'Jaguar' => "https://upload.wikimedia.org/wikipedia/en/b/b4/Jaguar_2012_logo.png",
        'Jac' => "https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/Logo_jac.jpg/220px-Logo_jac.jpg",
        'Isuzu' => "https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Isuzu.svg/200px-Isuzu.svg.png",
        'Faw' => "https://upload.wikimedia.org/wikipedia/en/thumb/6/64/Faw-group-logo.png/180px-Faw-group-logo.png",
        'Lada' => "https://upload.wikimedia.org/wikipedia/en/thumb/1/12/Lada_company_logo.png/220px-Lada_company_logo.png",
        'Kia' => "https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/KIA_logo2.svg/334px-KIA_logo2.svg.png",
        'MG' => "https://upload.wikimedia.org/wikipedia/en/6/66/New_mg_logo.png",
        'Dodge' => "https://upload.wikimedia.org/wikipedia/commons/d/d0/Dodge_logo_bars.png",
        'Chrysler' => "https://upload.wikimedia.org/wikipedia/en/thumb/e/ee/Chrysler_logo14.png/250px-Chrysler_logo14.png",
        'Great Wall' => "https://upload.wikimedia.org/wikipedia/commons/0/0b/Great_Wall_Motors_logo_1.png",
        'Daihatsu' => "https://upload.wikimedia.org/wikipedia/en/thumb/4/4f/Daihatsu.svg/250px-Daihatsu.svg.png",
        'Hummer' => "https://upload.wikimedia.org/wikipedia/commons/8/87/Hummer_wordmark.svg",
        'Renault' => "https://upload.wikimedia.org/wikipedia/commons/4/49/Renault_2009_logo.svg",
        // 'Peugeot' => "https://upload.wikimedia.org/wikipedia/commons/b/b8/Peugeot-nouveau-logo-640x360.jpg",
        'Opel' => "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7b/Opel-Logo_2017.png/1024px-Opel-Logo_2017.png",
        'Nissan' => "https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/Nissan-logo.svg/512px-Nissan-logo.svg.png",
        'Infiniti' => "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Infiniti_logo.svg/300px-Infiniti_logo.svg.png",
        'Mitsubishi' => "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Mitsubishi_logo.svg/500px-Mitsubishi_logo.svg.png",
        'Ford' => "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/Ford_Motor_Company_Logo.svg/304px-Ford_Motor_Company_Logo.svg.png",
        'Jeep' => "https://upload.wikimedia.org/wikipedia/en/d/d3/Volkswagen_logo.png",
        'Volkswagen' => "https://upload.wikimedia.org/wikipedia/en/d/d3/Volkswagen_logo.png",
        'Volvo' => "https://upload.wikimedia.org/wikipedia/commons/thumb/5/54/Volvo_logo.svg/800px-Volvo_logo.svg.png",
        'Toyota' => "https://upload.wikimedia.org/wikipedia/commons/9/9d/Toyota_carlogo.svg",
        'Land Cruiser' => "https://upload.wikimedia.org/wikipedia/commons/9/9d/Toyota_carlogo.svg",
        'Mazda' => "https://upload.wikimedia.org/wikipedia/en/1/18/Mazda_logo_with_emblem.svg",
        'Hyundai' => "https://upload.wikimedia.org/wikipedia/commons/4/44/Hyundai_Motor_Company_logo.svg",
        'Fiat' => "https://upload.wikimedia.org/wikipedia/en/9/96/Fiat_Logo.svg",
        'Ferrari' => "https://upload.wikimedia.org/wikipedia/en/thumb/d/d1/Ferrari-Logo.svg/344px-Ferrari-Logo.svg.png",
        'Honda' => "https://upload.wikimedia.org/wikipedia/commons/0/09/Honda-logo.svg",
        'Daewoo' => "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Daewoo_logo.svg/220px-Daewoo_logo.svg.png",
        'Citroen' => "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c8/Citroen_2016_logo.svg/244px-Citroen_2016_logo.svg.png",
        'Chevrolet' => "https://upload.wikimedia.org/wikipedia/en/e/ec/Chevrolet_logo13.png",
        'Cadillac' => "https://upload.wikimedia.org/wikipedia/en/d/de/Cadillac_logo.png",
        //~ 'BYD' => "https://upload.wikimedia.org/wikipedia/en/9/90/BuildYourDreams.svg",
        'BYD' => "https://duckduckgo.com/i/a471ba72.png",
        'Brilliance' => "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Brilliance_Auto_logo.svg/1024px-Brilliance_Auto_logo.svg.png",
        'BMW' => "https://upload.wikimedia.org/wikipedia/commons/4/44/BMW.svg",
        'Audi' => "https://upload.wikimedia.org/wikipedia/commons/9/92/Audi-Logo_2016.svg",
        'Mercedes' => "https://upload.wikimedia.org/wikipedia/en/b/bb/Mercedes_benz_silverlogo.png",
        'Alfa Romeo' => "https://upload.wikimedia.org/wikipedia/en/2/2a/Alfa_Romeo_logo.png",
      ];

      if(isset($logo_refs[$this->make]))
      {
        return $logo_refs[$this->make];
      } else {
        return false;
      }
    }
}
