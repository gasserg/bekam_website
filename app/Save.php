<?php

namespace scratch;

use Illuminate\Database\Eloquent\Model;

class Save extends Model
{
    protected $fillable = ['user_id', 'likable_id', 'likable_type'];

    public function likable()
    {
      return $this->morphTo();
    }

    public function user()
    {
      return $this->belongsTo('scratch\User');
    }
}
