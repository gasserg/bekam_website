<?php

namespace scratch;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

// Laravel Messenger
use Cmgmyr\Messenger\Traits\Messagable;

class User extends Authenticatable
{
    use Notifiable;
    use Messagable;

    protected $fillable = [
        'name', 'email', 'password', 'type', 'phone',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function comments()
    {
      return $this->hasMany('scratch\Comment');
    }

    public function cars()
    {
      return $this->hasMany('scratch\Car');
    }

    public function realestates()
    {
      return $this->hasMany('scratch\Realestate');
    }

    public function saves()
    {
      return $this->hasMany('scratch\Save');
    }

    public function points()
    {
      return $this->hasMany('scratch\Point');
    }

    public static function remainingPoints($id)
    {
      return 2 - static::find($id)->points()->sum('amount');
    }

}
