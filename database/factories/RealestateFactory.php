<?php

use Faker\Generator as Faker;

$factory->define(scratch\Realestate::class, function (Faker $faker) {
  return [
      'city' => 'City-'.random_int(1,10),
      'location' => 'Location-'.random_int(1,20),
      'price' => random_int(30000, 300000),
      'area' => random_int(90, 1200),
      'url' => [$faker->url, null][random_int(0,1)],
      'description' => $faker->paragraph,
      'user_id' => random_int(0,30),
      'post_type' => ['sale', 'sale', 'sale', 'rent'][random_int(0,3)],
      'unit_type' => ['apartment', 'apartment', 'apartment', 'villa'][random_int(0,3)],
  ];
});
