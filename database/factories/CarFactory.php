<?php

use Faker\Generator as Faker;

$factory->define(scratch\Car::class, function (Faker $faker) {
    return [
        'make' => 'Make '.str_random(10),
        'model' => 'Model '.str_random(10),
        'price' => random_int(30000, 300000),
        'year' => random_int(1980, 2018),
        'url' => [$faker->url, null][random_int(0,1)],
        'description' => $faker->paragraph,
        'user_id' => random_int(0,30),
    ];
});
