<?php

use Faker\Generator as Faker;

$factory->define(scratch\Comment::class, function (Faker $faker) {
    return [
        'user_id' => random_int(0,30),
        'body' => $faker->paragraph,
        'commentable_id' => random_int(0,50),
        'commentable_type' => ['scratch\Car', 'scratch\Realestate'][random_int(0,1)],
    ];
});
