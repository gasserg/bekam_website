<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealestatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('realestates', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('city');
            $table->string('location');
            $table->string('post_type');
            $table->string('unit_type');
            $table->integer('price');
            $table->integer('area');
            $table->text('description')->nullable();
            $table->integer('user_id')->nullable();
            $table->text('url')->nullable();
            $table->string('url_hash')->nullable()->unique();
            $table->integer('views')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('realestates');
    }
}
