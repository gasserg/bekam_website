<?php

use Illuminate\Database\Seeder;

class RealestatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(scratch\Realestate::class, 30)->create();
    }
}
