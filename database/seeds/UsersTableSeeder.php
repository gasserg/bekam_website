<?php

use Illuminate\Database\Seeder;

use scratch\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(['name' => 'bot', 'email' => 'bot@a.com', 'password' => bcrypt('ThisisMrBot1234knfds13lknasd32rklwn')]);
        User::create(['name' => 'gasser', 'email' => 'gasser@a.com', 'password' => bcrypt('g12345')]);
        factory(scratch\User::class, 50)->create();
    }
}
