<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$locale = Request::segment(1);

Route::get('/', function () {
    return redirect('/ar/');
});

if (in_array($locale, ['ar', 'en'])) {
    App::setLocale($locale);
} else {
    $locale = null;
}

Route::group(array('prefix' => $locale), function(){

  Auth::routes();

  Route::get('/', function () {
      return view('welcome');
  });

  Route::get('lang/{newLocale}', function($newLocale){
    $new_url = str_replace('/'.App::getLocale().'/', '/'.$newLocale.'/' , url()->previous().'/');
    $new_url = substr($new_url, 0, -1);
    return redirect($new_url);
  })->name('lang');

  Route::get('/user/edit', 'UserController@edit')->name('user.edit');
  Route::put('/user/update', 'UserController@update')->name('user.update');

  Route::get('/home', 'HomeController@index')->name('home');

  Route::get('/aboutandcontacts', 'AboutAndContactsController@index')->name('aboutandcontacts');

  Route::get('/cars/mycars', 'CarController@mycars')->name('cars.mycars');
  Route::get('/cars/saved', 'CarController@saved')->name('cars.saved');

  Route::get('/realestate/myrealestate', 'RealestateController@myrealestate')->name('realestate.myrealestate');
  Route::get('/realestate/saved', 'RealestateController@saved')->name('realestate.saved');

  Route::get('/user/{id}', "UserController@show")->name('user.show');

  Route::get('/notifications', "NotificationController@index")->name('notifications')->middleware('auth');

  Route::resource('/cars', 'CarController', ['except' => ['update', 'edit']]);
  Route::resource('/realestate', 'RealestateController', ['except' => ['update', 'edit']]);
  Route::resource('/comment', "CommentController", ['only' => ['store', 'destroy']]);
  Route::resource('/save', 'SaveController', ['only' => ['store', 'destroy']]);

  Route::group(['prefix' => 'messages', 'middleware' => ['web', 'auth', ]], function () {
      Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
      Route::get('create/{user_id}', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
      Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
      Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
      Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
  });

  Route::get('/sitemap', 'SitemapController@index')->name('sitemap.index');
  Route::get('/sitemap.xml', 'SitemapController@index');
  Route::get('/sitemap/main', 'SitemapController@main')->name('sitemap.main');
  Route::get('/sitemap/cars', 'SitemapController@cars')->name('sitemap.cars');
  Route::get('/sitemap/realestate', 'SitemapController@realestate')->name('sitemap.realestate');
});
