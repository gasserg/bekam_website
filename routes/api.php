<?php

use Illuminate\Http\Request;

use scratch\Car;
use scratch\User;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/cars/makes', "Api\CarController@indexMakes")->name("cars.indexMakes");
Route::any('/cars/models', "Api\CarController@indexModels")->name("cars.indexModels");

Route::get('/realestate/cities', "Api\RealestateController@indexCities")->name("realestate.indexCities");
Route::any('/realestate/locations', "Api\RealestateController@indexLocations")->name("realestate.indexLocations");

Route::get('/cars/count', 'CarController@count')->name('backcarscount');
Route::get('/realestate/count', 'RealestateController@count')->name('backrealestatecount');
