# Bekam?

## Description:
These are the files for the website Bekam? http://bekam-eg.com built using laravel.

## To Do List:
* [ ] Login and Register using social media
* [ ] User Admin Roles
* [ ] Admin Panel
* [ ] Points to be able to create ads
* [ ] Promoted Ads
* [ ] Payments
* [ ] Validation for the files when creating the posts: size, number, dimensions, type.
* [ ] Change title according to the current page
