<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <sitemap>
      <loc>{{ Route('sitemap.main') }}</loc>
      <lastmod>{{ Carbon\Carbon::today()->tz('UTC')->toAtomString() }}</lastmod>
  </sitemap>
    <sitemap>
        <loc>{{ Route('sitemap.cars') }}</loc>
        <lastmod>{{ Carbon\Carbon::today()->tz('UTC')->toAtomString() }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ Route('sitemap.realestate') }}</loc>
        <lastmod>{{ Carbon\Carbon::today()->tz('UTC')->toAtomString() }}</lastmod>
    </sitemap>
</sitemapindex>
