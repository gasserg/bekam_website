@extends('layouts.app')

@section('content')

@include('partials.sharingbuttons', ['post' => $car, ])

<div class="panel">
  <div class='row'>
    <div class='col-sm-12'>
      @include("partials.postdeletebutton", ['post' => $car, "destroy_route" => "cars.destroy"])
      @include("partials.postsavebutton", ['post' => $car, "likable_type" => "scratch\Car"])
      @if( $car->logo )
          <img class='logo-normal img thumbnail' src="{{ $car->logo }}" />
      @endif
      <h3>{{ $car->make }} - {{ $car->model }}</h3>
    </div>
  </div>

  <table>
    <tr>
      <td><strong>@lang('Make'):</strong></td>
      <td>{{ $car->make }}</td>
    </tr>
    <tr>
      <td><strong>@lang('Model'):</strong></td>
      <td>{{ $car->model }}</div>
    </tr>
    <tr>
      <td><strong>@lang('Year'):</strong></td>
      <td>{{ $car->year }}</td>
    </tr>
    <tr>
      <td><strong>@lang('Price'):</strong></td>
      <td>{{ number_format($car->price) }}</td>
    </tr>
    <tr>
      <td><strong>@lang('Description'):</strong></td>
      <td>{{ $car->description }}</td>
    </tr>
  </table>
  @include('partials.postnumberofviews', ['post' => $car, ])
  </div>

@include('partials.showpostuser', ['post' => $car, ])

@include('partials.showpostpictures', array('post' => $car))

@include('partials.showpostcomments', array('post' => $car, 'commentable_type'=>'scratch\Car'))

@endsection
