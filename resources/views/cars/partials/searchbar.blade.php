<a id='search-collapse-btn' class="collapsed visible-sm visible-xs btn btn-block" data-toggle="collapse" data-target="#search-form" aria-expanded="false">
  <span><strong>Search</strong></span><span class='glyphicon glyphicon-search'></span>
</a>

<div class="row">
  <div class='col-sm-12'>
    <div id='search-form' class="panel collapse">
      @include("partials.showerrors")
      <form class="form" action="{{ route('cars.index') }}" method='get'>
        <div class="row">
          <div class="col-sm-12">
            <div class="input-group">
              <span class="input-group-addon">@lang('Search')</span>
              <input type="text" class='form-control' name="q" placeholder="@lang('Search')" value="{{ old('q') }}" autocomplete="off">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="input-group">
              <span class="input-group-addon">@lang('Year')</span>
              <input type="number" class='form-control' name="min-year" placeholder="@lang('Minimum Year')" value="{{ old('min-year') }}" min=1900>
              <input type="number" class='form-control' name="max-year" placeholder="@lang('Maximum Year')" value="{{ old('max-year') }}" min=1900>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="input-group">
              <span class="input-group-addon">@lang('Price')</span>
              <input type="number" class='form-control' name="min-price" placeholder="@lang('Minimum Price')" value="{{ old('min-price') }}" min=0>
              <input type="number" class='form-control' name="max-price" placeholder="@lang('Maximum Price')" value="{{ old('max-price') }}" min=0>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="input-group">
              <span class="input-group-addon">@lang('Manufacturer')</span>
              <select id="cars-search-bar-make" class='form-control' name="make" value="{{ old('make') }}">
                <option value="">All</option>
                @foreach(scratch\Car::select('make')->distinct()->orderBy('make')->get() as $make)
                  @if($make['make'] == old('make'))
                    <option value="{{ $make['make'] }}" selected>{{ $make['make'] }}</option>
                  @else
                    <option value="{{ $make['make'] }}">{{ $make['make'] }}</option>
                  @endif
                @endforeach
              </select>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="input-group">
              <span class="input-group-addon">@lang('Model')</span>
              @if(old('make') == null)
                <select id="cars-search-bar-model" class='form-control' name="model" value="{{ old('model') }}" disabled>
                  <option value="">@lang('Please select a model first')</option>
                </select>
              @else
                <select id="cars-search-bar-model" class='form-control' name="model" value="{{ old('model') }}">
                  <option value="">All</option>
                  @foreach(scratch\Car::select('model')->where('make', old('make'))->distinct()->get() as $model)
                    @if($model['model'] == old('model'))
                      <option value="{{ $model['model'] }}" selected>{{ $model['model'] }}</option>
                    @else
                      <option value="{{ $model['model'] }}">{{ $model['model'] }}</option>
                    @endif
                  @endforeach
              @endif
              </select>
            </div>
          </div>
        </div>


        <input type="submit" class="btn btn-sm btn-primary btn-block" value="@lang('Search')" />

      </form>
    </div>
  </div>
</div>
