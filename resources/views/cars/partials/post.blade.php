<div class="post panel" onclick="window.location.href = '{{ route('cars.show', $car->id) }}'">
  <div style="width:100%;">

    @include("partials.postdeletebutton", ['post' => $car, "destroy_route" => "cars.destroy"])
    @include("partials.postsavebutton", ['post' => $car, "likable_type" => "scratch\Car"])
    @include("partials.externalPostBadge", ['post' => $car, ])

    <h4>
      @if( $car->logo )
        <img class='logo-small img' src="{{ $car->logo }}" />
      @endif
      <a href = "{{ route('cars.show', $car->id) }}">
        {{ $car->make }} - {{ $car->model }} - {{ $car->year }}
      </a>
    </h4>

    <p>@lang('Price') : {{ number_format($car->price) }} EGP</p>

  </div>

  <div>
    @if(strlen($car->description) > 200)
    {{ substr($car->description, 0, 200)."..." }}
    @else
    {{ $car->description }}
    @endif
  </div>

</div>
