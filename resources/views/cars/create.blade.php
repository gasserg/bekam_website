@extends('layouts.app')

@section('content')
<h3>@lang('Post new Car AD')</h3>
@include("partials.showerrors")
<form method='POST' action="{{ route('cars.store') }}" class="form" enctype="multipart/form-data">
  {{ csrf_field() }}
  <div class='form-group'>
    <label for="make">@lang('Manufacturer'):</label>
    <input name="make" type="text" class="form-control" placeholder="@lang('Enter a Manufacturer')" value="{{ old('make') }}" required autocomplete="off">
    @if($errors->has('make'))
      <span class='help-block'>{{ $errors->first('make') }}</span>
    @endif
  </div>
  <div class='form-group'>
    <label for="model">@lang('Model'):</label>
    <input name="model" type="text" class="form-control" placeholder="@lang('Enter a Model')" value="{{ old('model') }}" required autocomplete="off">
    @if($errors->has('model'))
      <span class='help-block'>{{ $errors->first('model') }}</span>
    @endif
  </div>
  <div class='form-group'>
    <label for="year">@lang('Year'):</label>
    <input name="year" type="number" class="form-control" placeholder="@lang('Enter Year')" value="{{ old('year') }}" required autocomplete="off">
    @if($errors->has('year'))
      <span class='help-block'>{{ $errors->first('year') }}</span>
    @endif
  </div>
  <div class='form-group'>
    <label for="price">@lang('Price'):</label>
    <input name="price" type="number" class="form-control" placeholder="@lang('Enter Price')" value="{{ old('price') }}" required autocomplete="off">
    @if($errors->has('price'))
      <span class='help-block'>{{ $errors->first('price') }}</span>
    @endif
  </div>
  <div class='form-group'>
    <label for="Description:">@lang('Description'):</label>
    <textarea name="description" class="form-control" placeholder="@lang('Enter Description')" value="{{ old('description') }}" required autocomplete="off">
      {{ old('description') }}
    </textarea>
    @if($errors->has('description'))
      <span class='help-block'>{{ $errors->first('description') }}</span>
    @endif
  </div>
  @include('partials.fileuploadinput')
  <input type="submit" class="btn btn-primary btn-block" value="@lang('Submit')" />
</form>
@endsection
