@extends('layouts.app')

@section('content')

@include('cars.partials.searchbar')

<h2>@lang('Cars')</h2>

@if($cars->count() > 0)
@foreach ($cars as $car)
  @include('cars.partials.post', ['car' => $car, ])
@endforeach
@else
<div class="panel">
  <strong>@lang('No Results Found')</strong>
<div>
@endif

<div class="pagination-container">
  {{ $cars->links() }}
</div>

@endsection
