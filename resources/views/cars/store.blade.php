@extends('layouts.app')

@section('content')

<div class="alert alert-success">
  <a href="{{ route('cars.index') }}">
  <strong>
    @lang('Post Created!')
  </strong>
    @lang('Click Here to return to the list of Car ADs!')
  </a>
</div>

@endsection
