@extends('layouts.app')

@section('content')

@include('partials.sharingbuttons', ['post' => $realestate, ])

<div class="panel">
  <div class="row">
    <div class='col-sm-12'>
      @include("partials.postdeletebutton", ['post' => $realestate, "destroy_route" => "realestate.destroy"])
      @include("partials.postsavebutton", ['post' => $realestate, "likable_type" => "scratch\Realestate"])
      <h3>{{ substr($realestate->description, 0, 30)."..." }}</h3>
    </div>
  </div>
  <table>
    <tr>
      <td>
        <b>@lang('City'):</b>
      </td>
      <td>
        {{ $realestate->city }}
      </td>
    </tr>
    <tr>
      <td>
        <b>@lang('Location'):</b>
      </td>
      <td>
        {{ $realestate->location }}
      </td>
    </tr>
    <tr>
      <td>
        <b>@lang('Area'):</b>
      </td>
      <td>
        {{ $realestate->area }}
      </td>
    </tr>
    <tr>
      <td>
        <b>@lang('Price'):</b>
      </td>
      <td>
        {{ number_format($realestate->price) }} @lang('EGP')
      </td>
    </tr>
    <tr>
      <td>
        <b>@lang('Description'):</b>
      </td>
      <td>
        {{ $realestate->description }}
      </td>
    </tr>
    <tr>
      <td>
        <b>@lang('Post Type'):</b>
      </td>
      <td>
        {{ $realestate->post_type }}
      </td>
    </tr>
    <tr>
      <td>
        <b>@lang('Unit Type'):</b>
      </td>
      <td>
        {{ $realestate->unit_type }}
      </td>
    </tr>
  </table>
  @include('partials.postnumberofviews', ['post' => $realestate, ])
</div>

@include('partials.showpostuser', ['post' => $realestate, ])

@include('partials.showpostpictures', array('post' => $realestate))

@include('partials.showpostcomments', array('post' => $realestate, 'commentable_type'=>'scratch\Realestate'))

@endsection
