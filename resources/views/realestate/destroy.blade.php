@extends('layouts.app')

@section('content')

<div class="alert alert-success">
  <a href="{{ route('realestate.index') }}">
  <strong>
    @lang('Post Deleted Successfully!')
  </strong>
  @lang('Click Here to return to the list of Realestate ADs!')
  </a>
</div>

@endsection
