<div class="post panel" onclick="window.location.href = '{{ route('realestate.show', $realestate->id) }}'">
  <div style="width:100%;">
    <div>
      @include("partials.postdeletebutton", ['post' => $realestate, "destroy_route" => "realestate.destroy"])
      @include("partials.postsavebutton", ['post' => $realestate, "likable_type" => "scratch\Realestate"])
      @include("partials.externalPostBadge", ['post' => $realestate, ])
    </div>

    <div class="row"><div class='col-sm-12'>
      <a href="{{ route('realestate.show', $realestate->id) }}">
        <h4>{{ __($realestate->unit_type) }} - {{ __($realestate->post_type) }} - {{ $realestate->city }} - {{ $realestate->location }}</h4>
      </a>
    </div></div>

    <p>
      <b>@lang('Area'):</b>
      <span>{{ number_format($realestate->area) }} sqm</span>
    </p>
    <p>
      <b>@lang('Price'):</b>
      <span>{{ number_format($realestate->price) }} EGP</span>
    </p>
  </div>
  <div>
    @if(strlen($realestate->description) > 200)
    {{ substr($realestate->description, 0, 200)."..." }}
    @else
    {{ $realestate->description }}
    @endif
  </div>
</div>
