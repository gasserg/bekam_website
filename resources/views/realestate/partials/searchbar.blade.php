<a id='search-collapse-btn' class="collapsed visible-sm visible-xs btn btn-block" data-toggle="collapse" data-target="#search-form" aria-expanded="false">
  <span><strong>Search</strong></span><span class='glyphicon glyphicon-search'></span>
</a>

<div class="row">
  <div class='col-sm-12'>
    <div id='search-form' class="panel collapse">
      @include("partials.showerrors")
      <form class="form" action="{{ route('realestate.index') }}" method='get'>
        <div class="row">
          <div class="col-sm-12">
            <div class="input-group">
              <span class="input-group-addon">@lang('Search')</span>
              <input type="text" class='form-control' name="q" placeholder="@lang('Search')" value="{{ old('q') }}" autocomplete="off">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="input-group">
              <span class="input-group-addon">@lang('Area') (m<sup>2</sup>)</span>
              <input type="number" class='form-control' name="min-area" placeholder="@lang('Minimum Area')" value="{{ old('min-area') }}" min=0>
              <input type="number" class='form-control' name="max-area" placeholder="@lang('Maximum Area')" value="{{ old('max-area') }}" min=0>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="input-group">
              <span class="input-group-addon">@lang('Price') (@lang('EGP'))</span>
              <input type="number" class='form-control' name="min-price" placeholder="@lang('Minimum Price')" value="{{ old('min-price') }}" min=0>
              <input type="number" class='form-control' name="max-price" placeholder="@lang('Maximum Price')" value="{{ old('max-price') }}" min=0>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="input-group">
              <span class="input-group-addon">@lang('Unit Type')</span>
              <select class='form-control' name="unit-type" value="{{ old('unit-type') }}">
                <option value="">All</option>
                @foreach(scratch\Realestate::select('unit_type')->distinct()->get() as $unit_type)
                  @if( $unit_type['unit_type'] == old('unit-type') )
                    <option value="{{ $unit_type['unit_type'] }}" selected >{{ $unit_type['unit_type'] }}</option>
                  @else
                    <option value="{{ $unit_type['unit_type'] }}">{{ $unit_type['unit_type'] }}</option>
                  @endif
                @endforeach
              </select>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="input-group">
              <span class="input-group-addon">@lang('Post Type')</span>
              <select class='form-control' name="post-type" value="{{ old('post-type') }}">
                <option value="">All</option>
                @foreach(scratch\Realestate::select('post_type')->distinct()->get() as $post_type)
                  @if( $post_type['post_type'] == old('post-type') )
                    <option value="{{ $post_type['post_type'] }}" selected >{{ $post_type['post_type'] }}</option>
                  @else
                    <option value="{{ $post_type['post_type'] }}">{{ $post_type['post_type'] }}</option>
                  @endif
                @endforeach
              </select>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="input-group">
              <span class="input-group-addon">@lang('City')</span>
              <select id='realestate-search-bar-city' class='form-control' name="city" value="{{ old('city') }}">
                <option value="">All</option>
                @foreach(scratch\Realestate::select('city')->distinct()->orderBy('city')->get() as $city)
                  @if($city['city'] == old('city'))
                    <option value="{{ $city['city'] }}" selected>{{ $city['city'] }}</option>
                  @else
                    <option value="{{ $city['city'] }}">{{ $city['city'] }}</option>
                  @endif
                @endforeach
              </select>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="input-group">
              <span class="input-group-addon">@lang('Location')</span>
              @if(old('city') !== null)
                <select id='realestate-search-bar-location' class='form-control' name="location" value="{{ old('location') }}">
                  <option value="">All</option>
                  @foreach(scratch\Realestate::select('location')->where('city', old('city'))->distinct()->get() as $location)
                    @if($location['location'] == old('location'))
                      <option value="{{ $location['location'] }}" selected>{{ $location['location'] }}</option>
                    @else
                      <option value="{{ $location['location'] }}">{{ $location['location'] }}</option>
                    @endif
                  @endforeach
                </select>
              @else
                <select id='realestate-search-bar-location' class='form-control' name="location" value="{{ old('location') }}" disabled>
                  <option value="">@lang('Please select a city first')</option>
                </select>
              @endif
            </div>
          </div>
        </div>

        <input type="submit" class="btn btn-sm btn-primary btn-block" value="@lang('Search')" />
      </form>

    </div>
  </div>
</div>
