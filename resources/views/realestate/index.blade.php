@extends('layouts.app')

@section('content')

@include('realestate.partials.searchbar')

<h2>@lang('Realestate')</h2>

@if($realestates->count() > 0)
@foreach ($realestates as $realestate)
  @include('realestate.partials.post', ['realestate' => $realestate, ])
@endforeach
@else

<div class="panel">
  <strong>@lang('No Results Found')</strong>
<div>
@endif

<div class="pagination-container">
  {{ $realestates->links() }}
</div>

@endsection
