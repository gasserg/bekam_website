@extends('layouts.app')

@section('content')
<h3>@lang('Post new Realestate AD')</h3>
@include("partials.showerrors")
<form method='POST' action="{{ route('realestate.store') }}" class="form" enctype="multipart/form-data">
  {{ csrf_field() }}
  <div class='form-group'>
    <label for="city">@lang('City'):</label>
    <input name="city" type="text" class="form-control" placeholder="@lang('Enter a City')" value="{{ old('city') }}" required autocomplete="off">
    @if($errors->has('city'))
      <span class='help-block'>{{ $errors->first('city') }}</span>
    @endif
  </div>
  <div class='form-group'>
    <label for="location">@lang('Location'):</label>
    <input name="location" type="text" class="form-control" placeholder="@lang('Enter location')" value="{{ old('location') }}" required autocomplete="off">
    @if($errors->has('location'))
      <span class='help-block'>{{ $errors->first('location') }}</span>
    @endif
  </div>
  <div class='form-group'>
    <label for="area">@lang('Area'):</label>
    <input name="area" type="number" class="form-control" placeholder="@lang('Enter area')" value="{{ old('area') }}" required autocomplete="off">
    @if($errors->has('area'))
      <span class='help-block'>{{ $errors->first('area') }}</span>
    @endif
  </div>
  <div class='form-group'>
    <label for="price">@lang('Price'):</label>
    <input name="price" type="number" class="form-control" placeholder="@lang('Enter Price')" value="{{ old('price') }}" required autocomplete="off">
    @if($errors->has('price'))
      <span class='help-block'>{{ $errors->first('price') }}</span>
    @endif
  </div>
  <div class='form-group'>
    <label for="description">@lang('Description'):</label>
    <textarea name="description" class="form-control" placeholder="@lang('Enter Description')" value="{{ old('description') }}" required autocomplete="off">
      {{ old('description') }}
    </textarea>
    @if($errors->has('description'))
      <span class='help-block'>{{ $errors->first('description') }}</span>
    @endif
  </div>

  <div class='form-group'>
    <label for="post_type">@lang('Post Type'):</label>
    <select name="post_type" class="form-control" value="{{ old('post_type') }}" required>
      <option value=''>@lang('Please select an option')</option>
      <option value='sale'>@lang('For Sale')</option>
      <option value='rent'>@lang('For Rent')</option>
    </select>
    @if($errors->has('post_type'))
      <span class='help-block'>{{ $errors->first('post_type') }}</span>
    @endif
  </div>

  <div class='form-group'>
    <label for="unit_type">Unit Type:</label>
    <select name="unit_type" class="form-control" value="{{ old('unit_type') }}" required>
      <option value=''>@lang('Please select an option')</option>
      <option value='apartment'>@lang('Apartment')</option>
      <option value='villa'>Villa</option>
      <option value='shop'>@lang('Shop')</option>
      <option value='office'>@lang('Office')</option>
      <option value='twinhouse'>Twin House</option>
      <option value='chalet'>Chalet</option>
      <option value='penthouse'>Penthouse</option>
      <option value='duplex'>Duplex</option>
      <option value='land'>@lang('Land')</option>
      <option value='building'>Building</option>
      <option value='farm'>@lang('Farm')</option>
      <option value='factory'>@lang('Factory')</option>
      <option value='storage'>@lang('Storage')</option>
    </select>
    @if($errors->has('unit_type'))
      <span class='help-block'>{{ $errors->first('unit_type') }}</span>
    @endif
  </div>


  @include('partials.fileuploadinput')
  <input type="submit" class="btn btn-primary btn-block" value="@lang('Submit')">
</form>
@endsection
