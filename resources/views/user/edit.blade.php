@extends('layouts.app')

@section('content')

<div class='row'>
  <div class='col-md-8 col-md-offset-2'>
    <div class="panel">
      @if(isset($user_edit_success))
        @if($user_edit_success)
          <div class='alert alert-success'>
            Sucess! <a href="/">Click here to return to home.</a>
          </div>
        @endif
      @endif

      <form class='form' method='post' action="{{ route('user.update') }}">
        {{ csrf_field() }}
        {{ method_field('put') }}
        @include('partials.showerrors')
        <div class='row'>
          <div class='form-group'>
            <label class='col-sm-4 control-label' for='name'>User Name:</label>
            <div class='col-sm-8'>
              <input class='form-control' type='text' name='name' value='{{ Auth::user()->name }}' required autofocus autocomplete="off" />
            </div>
          </div>
        </div>

        <div class='row'>
          <div class='form-group'>
            <label class='col-sm-4 control-label' for='name'>email:</label>
            <div class='col-sm-8'>
              <input class='form-control' type='text' name='email' value='{{ Auth::user()->email }}' required autocomplete="off" />
            </div>
          </div>
        </div>

        <div class='row'>
          <div class='form-group'>
            <label class='col-sm-4 control-label' for='name'>Phone Number:</label>
            <div class='col-sm-8'>
              <input class='form-control' type='text' name='phone' value='{{ Auth::user()->phone }}' required autocomplete="off" />
            </div>
          </div>
        </div>

        <div class='row'>
          <div class='col-md-12'>
            <input type='submit' class='btn btn-primary btn-block' value='Submit' />
          </div>
        </div>

      </form>

    </div>
  </div>
</div>

@endsection
