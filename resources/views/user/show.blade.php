@extends('layouts.app')

@section('content')

<div class="panel">

<h3>{{ $user->name }}</h3>

  <div class="row">
    <div class="col-sm-3">
      <strong>email:</strong>
    </div>
    <div class="col-sm-9">
      {{ $user->email }}
    </div>
  </div>

  <div class="row">
    <div class="col-sm-3">
      <strong>Phone:</strong>
    </div>
    <div class="col-sm-9">
      @if($user->phone)
      {{ $user->phone }}
      @else
      Unknown
      @endif
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <a href="{{ route('messages.create', [$user->id, ]) }}" class="btn btn-block btn-primary">
        <span class="glyphicon glyphicon-envelope"></span>
        Send Private Message
      </a>
    </div>
  </div>

</div>

<h4>Cars Posted</h4>
@if($user->cars->count() > 0)
@foreach($user->cars as $car)
  @include('cars.partials.post', ['car' => $car, ])
@endforeach
@else
<div class='panel'>No Posts here yet...</div>
@endif

<h4>Realestate Posted</h4>
@if($user->realestates->count() > 0)
@foreach($user->realestates as $realestate)
  @include('realestate.partials.post', ['realestate' => $realestate, ])
@endforeach
@else
<div class='panel'>No Posts here yet...</div>
@endif

@endsection
