<div class="row">
  <div class="col-sm-12">
    <div class="alert alert-info">
      <span class="glyphicon glyphicon-info-sign"></span>
      {!! __("By Logging in or Creating a new account, you will be able to:<ul><li>Save posts and compare them later</li><li>Comment on ADs</li><li>Create you own ADs</li><li>Send Private messages</li></ul>") !!}
    </div>
  </div>
</div>
