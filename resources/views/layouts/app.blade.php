<!DOCTYPE html>
<html lang="{{ App::getLocale() }}" {{ (App::getLocale() == 'ar') ? "dir=rtl" : '' }}>

@include('partials.head')
<body>
  @include('partials.header')
    <div id="app">
        <div class="container">
            <div class='col-md-12'>
                @yield('content')
            </div>
        </div>
    </div>
    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
