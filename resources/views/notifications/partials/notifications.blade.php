@if(Auth::check())
<li>
  <a href="{{ route('notifications') }}">
    <span class="glyphicon glyphicon-bell" />
    <span class="badge">{{ Auth::user()->unreadNotifications->count() }}</span>
  </a>
</li>
@endif
