@extends('layouts.app')

@section('content')

@if($notifications->count() == 0)
<div class="panel">No notifications yet...</div>
@else
@foreach($notifications as $notification)
@if(isset($notification->data['message']) and isset($notification->data['url']))
<div class='post' onclick="window.location.href = '{{ url($notification->data['url']) }}'">
  @if($notification->read_at == null)
  <span class="badge badge-info pull-right">New</span>
  @endif
  <p>{{ $notification->data['message'] }}</p>
</div>
@endif
@endforeach
@endif

<div class="pagination-container">
  {{ $notifications->links() }}
</div>

{{ $notifications->markAsRead() }}

@endsection
