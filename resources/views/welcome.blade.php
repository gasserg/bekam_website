@extends('layouts.app')

@section('content')

@if( Auth::check() )
<div class='alert alert-info'>
  <strong>
    {{ __('messages.welcome', ['name' => Auth::user()->name, ]) }}
  </strong>
</div>
@endif

<div class='jumbotron' style="">
  <div class='row'>
    <div class='col-sm-12'>
      {!! __("<p><strong>Bekam?</strong> is a search engine / price aggregator for used cars and real estate in Egypt.</p><ul><li>Browse classified adverisements from several websites</li><li>Save ADs to compare later</li><li>Post your own ADs</li></ul>") !!}
    </div>
  </div>
  @if( ! Auth::check() )
  <div class='row'>
    <div class='col-sm-6'>
      <a rel="nofollow" href={{ route('login') }} class='btn btn-block btn-primary'>@lang('Login')</a>
    </div>
    <div class='col-sm-6'>
      <a rel="nofollow" href={{ route('register') }} class='btn btn-block btn-primary'>@lang('Register')</a>
    </div>
  </div>
  @endif
</div>

<div id='cars-jumbotron' class='jumbotron jumbotron-title jumbotron-clickable' onclick="window.location.href = '{{ route('cars.index') }}'">
  <h2><b><strong>@lang('Cars')</strong></b></h2>
  <h3><cars-count></cars-count></h3>
  <a href="{{ route('cars.index') }}" class="btn btn-primary">@lang('Browse used cars')</a>
</div>

<div id='realestate-jumbotron' class='jumbotron jumbotron-title jumbotron-clickable' onclick="window.location.href = '{{ route('realestate.index') }}'">
  <h2><b><strong>@lang('Real Estate')</strong></b></h2>
  <h3><realestate-count></realestate-count></h3>
  <a href="{{ route('realestate.index') }}" class="btn btn-primary">@lang('Browse real estate') <span class='label label-warning'>BETA</span></a>
</div>

@include('partials.footer')

@endsection
