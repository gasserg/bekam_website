@if(Auth::check())
@if($post->user_id != Auth::user()->id)
<span class="{{ (App::getLocale() == 'ar') ? 'pull-left' : 'pull-right' }}">
  @if($post->saves->where('user_id', Auth::user()->id)->count() == 0)
  <form method='post' action="{{ route('save.store') }}" class="form">
    {{ csrf_field() }}
    {{ method_field('post') }}
    <input type="hidden" name="likable_id" value="{{ $post->id }}" />
    <input type="hidden" name="likable_type" value="{{ $likable_type }}" />
    <input class="btn btn-sm btn-primary" type="submit" value="Save"/>
  </form>
  @else
  <form method='post' action="{{ route('save.destroy', $post->saves->where('user_id', Auth::user()->id)->first->id) }}" class="form">
    {{ csrf_field() }}
    {{ method_field('delete') }}
    <input class="btn btn-sm btn-warning" type="submit" value="Un-Save"/>
  </form>
  @endif
</span>
@else
<span class='pull-right badge badge-info' style='background-color: green; margin: 0.1em;'>This post is yours</span>
@endif
@endif
