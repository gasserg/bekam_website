<div class="row" style="color: grey; font-size:0.9em;">
  <div class="col-sm-12">
    <span class="pull-right">
    <strong>@lang('Number of Views'):</strong> {{ $post->views }}
  </span>
  </div>
</div>
