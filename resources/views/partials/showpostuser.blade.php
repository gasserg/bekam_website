<div class="panel">
<div class="row">
  <div class="col-sm-12">
  <p>
    @if($post->user)
      @if(Auth::check())
        <a href="{{ route('user.show', $post->user->id) }}">{{ $post->user->name }}</a>
      @else
        {{ $post->user->name }}
      @endif
    @else
      @lang('Unknown')
    @endif
    </p>
    <p>
      {{ $post->created_at }}
    </p>
  <p>
  @if($post->user)
    @if($post->user->phone)
      @if( Auth::check() )
        {{ $post->user->phone }}
      @else
        <b><a href="{{ route('login') }}">Login</a> or <a href="{{ route('register') }}">Register</a> to see the phone number</b>
      @endif
    @else
      @lang('Unknown')
    @endif
  </p>

  <p>
    @if(Auth::check())
    <a href="{{ route('messages.create', [$post->user->id, ]) }}" class="btn btn-block btn-primary btn-sm">
      <span class="glyphicon glyphicon-envelope"></span>
      Send Private Message
    </a>
    @else
    <div class='alert alert-info'>
    <b><span class="glyphicon glyphicon-envelope"></span> <a href="{{ route('login') }}">@lang('Login')</a> @lang('or') <a href="{{ route('register') }}">@lang('Register')</a></b>
    </div>
    @endif
  </p>

@endif
  </div>
</div>
</div>

@if($post->url)
<div class="row">
  <div class="col-sm-12">
    <a class="btn btn-primary btn-sm btn-block" href="{{ $post->url }}" target="_blank">@lang('Follow Post') <span class="label label-warning">Extenal Link</span></a>
  </div>
</div>
@endif
