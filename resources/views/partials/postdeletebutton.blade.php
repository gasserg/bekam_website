@auth
@if($post->user['id'] == Auth::id())
<span class="{{ (App::getLocale() == 'ar') ? 'pull-left' : 'pull-right' }}">
  <form action="{{ route($destroy_route, $post->id) }}" method="post">
    {{ method_field('delete') }}
    {{ csrf_field() }}
    <input class="btn btn-sm btn-danger" type="submit" value="Delete Post">
  </form>
</span>
@endif
@endauth
