@if($post->url)
<span class="badge {{ (App::getLocale() == 'ar') ? 'pull-left' : 'pull-right' }}">
{{ str_replace('www.', '', explode('/', explode('//', $post->url)[count(explode('//', $post->url))-1])[0]) }}
</span>
@endif
