<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container-fluid">
      <div class='navbar-header'>
        <a href="{{ url('/') }}">
           <img style='height:3em;' src="{{ asset('/media/brand.png') }}" alt="Bekam?" />
        </a>

        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
      </div>
        <div class="nav collapse navbar-collapse" id="app-navbar-collapse">

            <ul class="nav navbar-nav navbar-right">
              <li>
                <a href="{{ url('/') }}">
                  @lang('Home') <span class="glyphicon glyphicon-home" />
                </a>
              </li>

              @if(Auth::check())
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                  @lang('Cars') <span class="glyphicon glyphicon-tags" />
                  <span class="caret"></span>
                </a>
                <ul class='dropdown-menu'>
                  <li><a href="{{ route('cars.index') }}">@lang('Cars')</a></li>
                  <li><a href="{{ route('cars.mycars') }}">@lang('My cars')</a></li>
                  <li><a href="{{ route('cars.saved') }}">@lang('Saved Cars')</a></li>
                  <li><a href="{{ route('cars.create')}}">@lang('Post a new Car Ad')</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                  @lang('Realestate') <span class="glyphicon glyphicon-tags" />
                  <span class="caret"></span>
                </a>
                <ul class='dropdown-menu'>
                  <li><a href="{{ route('realestate.index') }}">@lang('Realestate')</a></li>
                  <li><a href="{{ route('realestate.myrealestate') }}">@lang('My Realestate')</a></li>
                  <li><a href="{{ route('realestate.saved') }}">@lang('Saved Realestate')</a></li>
                  <li><a href="{{ route('realestate.create')}}">@lang('Post a new Realestate AD')</a></li>
                </ul>
              </li>
              @else
              <li>
                <a href="{{ route('cars.index') }}">
                  @lang('Cars') <span class="glyphicon glyphicon-tags" />
                </a>
              </li>
              <li>
                <a href="{{ route('realestate.index') }}">
                  @lang('Realestate') <span class="glyphicon glyphicon-tags" />
                </a>
              </li>
              @endif

              @guest
                <li>
                  <a rel="nofollow" href="{{ route('login') }}">
                    @lang('Login') <span class="glyphicon glyphicon-log-in" />
                  </a>
                </li>
                <li>
                  <a rel="nofollow" href="{{ route('register') }}">
                    @lang('Register') <span class="glyphicon glyphicon-pencil" />
                  </a>
                </li>
              @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                          <a rel="nofollow" href="{{ route('user.edit') }}">
                            @lang('Change Settings') <span class="glyphicon glyphicon-wrench" />
                          </a>
                        </li>
                        <li>
                            <a rel="nofollow" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                @lang('Logout') <span class="glyphicon glyphicon-log-out" />
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                        <li>
                    </ul>
                </li>
                @endguest

                @if(App::getLocale() !== 'ar')
                <li>
                  <a href="{{ route('lang', 'ar') }}">
                    عربى <span class="glyphicon glyphicon-font" />
                  </a>
                </li>
                @endif
                @if(App::getLocale() !== 'en')
                <li>
                  <a href="{{ route('lang', 'en') }}">
                    English <span class="glyphicon glyphicon-font" />
                  </a>
                </li>
                @endif

                @include('notifications.partials.notifications')
                @include('messenger.partials.headernotification')
              </ul>
            </div>
        </span>
    </div>
</nav>
