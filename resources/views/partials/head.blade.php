<head>
  @if(env('APP_ENV') != 'local')
  <meta name="google-site-verification" content="ME8lgOA7GTaO6UVcirAmQrnzgk4Yb4ZHbG8tkn52y8U" />
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-111496505-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-111496505-1');
  </script>
  @endif

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  @if( isset($page_title) )
    <title>@lang('Bekam? Cars and Realestate') - {{ __($page_title) }}</title>
    <meta name="description" content="@lang('Bekam? bekam-eg.com is Search Engine for Used Cars and Realestate in Egypt. Compare Prices. Create your own ads.') {{ $page_title }}">
  @else
    <title>@lang('Bekam? Cars and Realestate')</title>
    <meta name="description" content="@lang('Bekam? bekam-eg.com is Search Engine for Used Cars and Realestate in Egypt. Compare Prices. Create your own ads.')">
  @endif

  <link href="{{ mix('css/app.css') }}" rel="stylesheet">

  <meta name="keywords" content="Bekam, , Egypt, search, engine, market, analysis, used, cars, realestate, sale, rent">

  <meta name="author" content="Gasser">
  <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/public/icons/apple-touch-icon.png?v=PY4BxxjdKv') }}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/public/icons/favicon-32x32.png?v=PY4BxxjdKv') }}">
  <link rel="icon" type="image/png" sizes="194x194" href="{{ asset('/public/icons/favicon-194x194.png?v=PY4BxxjdKv') }}">
  <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('/public/icons/android-chrome-192x192.png?v=PY4BxxjdKv') }}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/public/icons/favicon-16x16.png?v=PY4BxxjdKv') }}">
  <link rel="manifest" href="{{ asset('/public/icons/manifest.json?v=PY4BxxjdKv') }}">
  <link rel="mask-icon" href="{{ asset('/public/icons/safari-pinned-tab.svg?v=PY4BxxjdKv') }}" color="#000000">
  <link rel="shortcut icon" href="{{ asset('/public/icons/favicon.ico?v=PY4BxxjdKv') }}">
  <meta name="apple-mobile-web-app-title" content="Bekam?">
  <meta name="application-name" content="Bekam?">
  <meta name="msapplication-TileColor" content="#000000">
  <meta name="msapplication-TileImage" content="{{ asset('/public/icons/mstile-144x144.png?v=PY4BxxjdKv') }}">
  <meta name="msapplication-config" content="{{ asset('/public/icons/browserconfig.xml?v=PY4BxxjdKv') }}">
  <meta name="theme-color" content="#ffffff">
</head>
