<div class="form-group">
  <label for="files">@lang('Photos'):</label>
  <input class='btn btn-default btn-file btn-block' name="files[]" type="file" accept="image/*"  multiple/>
</div>
