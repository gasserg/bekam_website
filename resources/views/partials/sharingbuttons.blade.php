
<social-sharing
                      url="{{ Request::fullUrl() }}"
                      title="{{ 'Bekam? '. $post->description }}"
                      description="{{ 'Bekam? Used Cars and Realestate. '. $post->description }}"
                      inline-template>
  <div style="text-align: right;">
      <network class='btn btn-xs btn-primary' network="email">
          <i class="fa fa-envelope"></i> Email
      </network>
      <network class='btn btn-xs btn-primary' network="facebook">
        <i class="fa fa-facebook"></i> Facebook
      </network>
      <network class='btn btn-xs btn-primary' network="googleplus">
        <i class="fa fa-google-plus"></i> Google +
      </network>
      <network class='btn btn-xs btn-primary' network="line">
        <i class="fa fa-line"></i> Line
      </network>
      <network class='btn btn-xs btn-primary' network="twitter">
        <i class="fa fa-twitter"></i> Twitter
      </network>
      <network class='btn btn-xs btn-primary' network="whatsapp">
        <i class="fa fa-whatsapp"></i> Whatsapp
      </network>
  </div>
</social-sharing>
