<div class='panel'>
  <h4>@lang('Photos'):</h4>
  <div class="row">
  @if($post->postfiles->isEmpty())
  <div class='col-md-12'><span>@lang('No pictures available')</span></div>
  @else
  @foreach($post->postfiles as $file)
    <div class="col-md-4 text-center">
      <a id="{{ $file->location }}" href="{{ url('/storage'.$file->location) }}">
        <img src="{{ url('/storage'.$file->location) }}" class="img img-thumbnail img-responsive" />
        <div class="modal fade">
          <img src="{{ url('/storage'.$file->location) }}" class="img img-thumbnail img-responsive" />
        </div>
      </a>
    </div>
  @endforeach
  @endif
  </div>
</div>
