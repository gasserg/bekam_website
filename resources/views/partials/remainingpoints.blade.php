@if(Auth::check())
<div class="points-counter">
  <span class="points-counter-title">Points</span>
  <span class="points-counter-number">{{ scratch\User::remainingPoints(Auth::id()) }}</span>
  <span class="points-counter-information">This is the amount of points remaining that you have<span>
</div>

<style>

.points-counter {
  background-color: white;
  border: 3px solid green;
  border-radius: 50em;
  text-align: center;
  box-shadow: 1px 1px 2px grey;
  width: 5em;
  padding: 0.5em;
  margin: 0.5em;
  position: fixed;
  bottom: 0;
  right: 0;
}

.points-counter-title {
  color: black;
  text-align: center;
  position: absolute;
  font-family: sans-serif;
  color: darkgrey;
}

.points-counter-number {
  font-size: 5em;
  text-align: center;
  text-shadow: 1px 1px 2px grey;
}

.points-counter-information {
  position: absolute;
  right: 0;
  bottom: 0;
  background-color: white;
  border: 1px solid grey;
  visibility: hidden;
  border-radius: 5px;
  box-shadow: 1px 1px 2px grey;
}

.points-counter:hover .points-counter-information {
  position: absolute;
  right:0;
  visibility: visible;
}

</style>

@endif
