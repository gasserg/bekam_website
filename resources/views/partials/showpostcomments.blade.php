<div class="panel">
  <h4>@lang('Comments'):</h4>
  @if($post->comments->isEmpty())
  <span>@lang('None has posted a comment yet')</span>
  @else
  @foreach($post->comments as $comment)
  <div class="comment">
      <div>
        <b>
          @if($comment->user)
          {{ $comment->user->name }}
          @else
          @lang('Unknown')
          @endif
        </b>
        on
        <b>{{ $comment->created_at }}:</b>
      </div>
      <div>{{ $comment->body }}</div>
  </div>
  @endforeach
  @endif

  <div class="seperator"></div>

@if(Auth::check())
  <h4>@lang('Create Comment')</h4>
  <form action="{{ route('comment.store')}}" method='post' class='form'>
    {{ csrf_field() }}
    {{ method_field('post') }}
    <input type="hidden" name="commentable_id" value="{{ $post->id }}">
    <input type="hidden" name="commentable_type" value="{{ $commentable_type }}">
    <input type="text" class="form-control" name="body" value="{{ old('body') }}" placeholder="@lang('Write comment here')" autocomplete="off">
    <input type="submit" class="btn btn-primary btn-sm btn-block" value="@lang('Submit Comment')">
  </form>
</div>
@else
<h4>@lang('Create Comment')</h4>
<div class='form'>
  <input type="text" class="form-control disabled" name="body" placeholder="Please login or create an account to comment" autocomplete="off" disabled>
</form>
</div>

@include('auth.partials.account_motivation')

@endif
