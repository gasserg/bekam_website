@extends('layouts.app')

@section('content')

<div class="panel">
  <p>
    <strong>Bekam?</strong> is a search engine and price aggregator for used cars and realestate. It searches for classified ads for used cars and realestate, in Egypt, and gathers the data here. By using this website, you can easily find what you're looking for on several websites, compare results
  </p>
  <p>
    For any suggestions, feedback, or anything, please
    <a href="mailto:info@bekam-eg.com?subject=Hello!
&body=">e-mail: info@bekam-eg.com</a>.
  </p>
</div>

@endsection
