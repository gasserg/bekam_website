@extends('layouts.app')

@section('content')

<div class="panel">
    <p><strong>Send message to <b><a href="{{ route('user.show', [$recipient->id, ]) }}">{{ $recipient->name }}</a></b></strong></p>
    <form action="{{ route('messages.store') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="recipient_id" value="{{ $recipient->id }}" />
            <!-- Subject Form Input -->
            <div class="form-group">
                <label class="control-label">Subject</label>
                <input type="text" class="form-control" name="subject" placeholder="Subject"
                       value="{{ old('subject') }}">
            </div>

            <!-- Message Form Input -->
            <div class="form-group">
                <label class="control-label">Message</label>
                <textarea name="message" class="form-control" placeholder="Write your message here...">{{ old('message') }}</textarea>
            </div>

            <!-- Submit Form Input -->
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control">Submit</button>
            </div>
    </form>
</div>
@stop
