@if(Auth::check())
<li>
  <a href="{{ route('messages') }}">
    <span class="glyphicon glyphicon-envelope" />
    <span class="badge">{{ Auth::user()->unreadMessagesCount() }}</span>
  </a>
</li>
@endif
