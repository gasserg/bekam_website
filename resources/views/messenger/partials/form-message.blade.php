<div class='panel'>
  <h3>New message</h3>
  <form action="{{ route('messages.update', $thread->id) }}" method="post">
      {{ method_field('put') }}
      {{ csrf_field() }}
      <div class="form-group">
          <textarea name="message" class="form-control" placeholder="Write your message here..." required >{{ old('message') }}</textarea>
      </div>
      <div class="form-group">
          <button type="submit" class="btn btn-primary form-control">Submit</button>
      </div>
  </form>

</panel>
