@extends('layouts.app')

@section('content')

    <h3>Messages</h3>

    @include('messenger.partials.flash')

    @each('messenger.partials.thread', $threads, 'thread', 'messenger.partials.no-threads')

@stop
