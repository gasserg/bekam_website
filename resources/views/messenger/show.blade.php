@extends('layouts.app')

@section('content')
    <div class="panel">
        <h3>{{ $thread->subject }}</h3>
        <p>with <b>
          @foreach($thread->participants as $p)
            @if($p->user_id !== Auth::id())
              <a href="{{ route('user.show', [$p->user_id, ]) }}" >{{ scratch\User::findOrFail($p->user_id)->name }}</a>
            @endif
          @endforeach
        </b></p>
        @each('messenger.partials.messages', $thread->messages, 'message')
    </div>

      @include('messenger.partials.form-message')
@stop
