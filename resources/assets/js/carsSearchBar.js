
// Manages the <select> in the search bar for cars.


$('#cars-search-bar-make').on('change', function(e){
  var self = $(this);
  var model = $('#cars-search-bar-model')
  model.html("<option value=''>Please wait...</option>");
  model.attr("disabled", true);
  if(self.val())
  {
    $.post("/api/cars/models",
      {
        make: $(this).val(),
      },
      function(data, status){
        model.html("<option value=''>All</option>");
        data.forEach(function(i){
          model.append("<option value='" + i.model + "'>" + i.model + "</option>");
        });
        model.attr('disabled', false);
    });
  } else {
    model.html("<option value=''>Please select a manufacturer first</option>");
  }
});
