
$("#search-form").on("hide.bs.collapse", function(){
  $("#search-collapse-btn").html("<span class='glyphicon glyphicon-collapse-down'></span> Open Search");
});
$("#search-form").on("show.bs.collapse", function(){
  $("#search-collapse-btn").html("<span class='glyphicon glyphicon-collapse-up'></span> Close Search");
});
if ( ! ($("#search-collapse-btn").is(":visible") ) )
{
  $("#search-form").addClass('in');
}
