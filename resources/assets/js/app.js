
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

var SocialSharing = require('vue-social-sharing');
Vue.use(SocialSharing);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('cars-count', require('./components/CarsCount.vue'));
Vue.component('realestate-count', require('./components/RealestateCount.vue'));

const app = new Vue({
    el: '#app'
});

// Require my files :D

require('./collapsible_searchbar.js');
require('./navButtonsToggle.js');
require('./carsSearchBar.js');
require('./realestateSearchBar');
