
// Manages the <select> in the search bar for realestate.

$('#realestate-search-bar-city').on('change', function(e){
  var self = $(this);
  var location = $('#realestate-search-bar-location');
  location.html("<option value=''>Please wait...</option>");
  location.attr("disabled", true);
  if(self.val())
  {
    $.post("/api/realestate/locations",
      {
        city: $(this).val(),
      },
      function(data, status){
        location.html("<option value=''>All</option>");
        data.forEach(function(i){
          location.append("<option value='" + i.location + "'>" + i.location + "</option>");
        });
        location.attr('disabled', false);
    });
  } else {
    location.html("<option val=''>Please select a city first...</option>");
  }
});
